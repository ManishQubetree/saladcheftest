﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResetColor : MonoBehaviour
{
    private void OnEnable()
    {
        GetComponent<UnityEngine.UI.Image>().color = Color.white;
    }
}
