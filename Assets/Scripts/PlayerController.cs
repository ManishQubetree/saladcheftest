﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{

    [SerializeField]
    KeyCode left, right, up, down, pickup, release;

    [SerializeField]
    float moveSpeed = 100;

    private Transform playerTransform;

    private GameObject activeObject;

    private string collectedItems;

    private Transform vegMenu;

    private bool isChopping = false;

    private void Start()
    {
        playerTransform = transform;
        vegMenu = playerTransform.GetChild(0);
    }

    private void Update()
    {
        if (isChopping)
            return;

        if (Input.GetKey(left))
        {
            playerTransform.position = GameManager.instance.GetClampedPlayerPosition(playerTransform.position + Vector3.left * (moveSpeed * Time.deltaTime));
        }
        else if (Input.GetKey(right))
        {
            playerTransform.position = GameManager.instance.GetClampedPlayerPosition(playerTransform.position + Vector3.right * (moveSpeed * Time.deltaTime));
        }
        else if (Input.GetKey(up))
        {
            playerTransform.position = GameManager.instance.GetClampedPlayerPosition(playerTransform.position + Vector3.up * (moveSpeed * Time.deltaTime));
        }
        else if (Input.GetKey(down))
        {
            playerTransform.position = GameManager.instance.GetClampedPlayerPosition(playerTransform.position + Vector3.down * (moveSpeed * Time.deltaTime));
        }
        else if (Input.GetKeyDown(pickup))
        {
            if (activeObject != null)
            {
                if (activeObject.layer == 10) //vegetable
                {
                    collectedItems += activeObject.name;
                    vegMenu.GetChild(int.Parse(activeObject.name)).gameObject.SetActive(true);
                    //Debug.Log(collectedItems);
                }
                else if (activeObject.layer == 11 || activeObject.layer == 12) //chopper
                {
                    for (int i = 0; i < 6; i++)
                    {
                        if (activeObject.transform.GetChild(0).GetChild(i).gameObject.activeSelf)
                        {
                            activeObject.transform.GetChild(0).GetChild(i).GetComponent<Image>().color = Color.white;
                            activeObject.transform.GetChild(0).GetChild(i).gameObject.SetActive(false);
                            int index = int.Parse(activeObject.transform.GetChild(0).GetChild(i).name);
                            collectedItems += index;
                            vegMenu.GetChild(index).gameObject.SetActive(true);
                            vegMenu.GetChild(index).gameObject.GetComponent<Image>().color = GameManager.instance.choppedColor;
                            break;
                        }
                    }
                }
            }
        }
        else if (Input.GetKeyDown(release))
        {
            if (activeObject != null)
            {
                if (activeObject.layer == 10 && collectedItems != null && collectedItems.Length > 0) //vegetable
                {
                    if (vegMenu.GetChild(int.Parse(activeObject.name)).GetComponent<Image>().color != GameManager.instance.choppedColor)
                    {
                        vegMenu.GetChild(int.Parse(activeObject.name)).gameObject.SetActive(false);
                        collectedItems = collectedItems.Remove(0, 1);
                        //Debug.Log(collectedItems);
                    }
                }
                else if ((activeObject.layer == 11 || activeObject.layer == 12) && collectedItems.Length > 0) //chopper
                {
                    int vegIndex = int.Parse(collectedItems.Substring(0, 1));
                    if (vegMenu.GetChild(vegIndex).gameObject.GetComponent<Image>().color != GameManager.instance.choppedColor)
                    {
                        vegMenu.GetChild(vegIndex).gameObject.SetActive(false);
                        activeObject.transform.GetChild(0).GetChild(vegIndex).gameObject.SetActive(true);
                        isChopping = true;
                        activeObject.transform.GetChild(0).GetChild(vegIndex).gameObject.AddComponent<Cook>().SetPlayerController(this);
                    }
                    collectedItems = collectedItems.Remove(0, 1);
                }
                else if (activeObject.layer == 15) // customer
                {
                    if (collectedItems.Length > 0)
                    {
                        for (int i = 0; i < 6; i++)
                        {
                            if (vegMenu.GetChild(i).gameObject.activeSelf && activeObject.transform.GetChild(1).GetChild(i).gameObject.activeSelf && vegMenu.GetChild(i).GetComponent<Image>().color == GameManager.instance.choppedColor)
                            {
                                vegMenu.GetChild(i).GetComponent<Image>().color = Color.white;
                                vegMenu.GetChild(i).gameObject.SetActive(false);
                                activeObject.transform.GetChild(1).GetChild(i).gameObject.SetActive(false);
                                activeObject.SendMessage("ItemDelivered");
                                collectedItems = collectedItems.Remove(0, 1);
                                break;
                            }
                        }
                    }
                }
            }
        }

    }

    public void SetChoppingDone()
    {
        isChopping = false;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        collision.gameObject.GetComponent<Outline>().enabled = true;
        activeObject = collision.gameObject;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        collision.gameObject.GetComponent<Outline>().enabled = false;
        activeObject = null;
    }
}
