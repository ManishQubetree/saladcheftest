﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Customer : MonoBehaviour
{
    private Image progressImage;
    private Vector3 spawnPosition;

    private int itemsCount;
    private Coroutine progressRoutine;

    private void Awake()
    {
        spawnPosition = transform.position;
        progressImage = transform.GetChild(0).GetChild(0).GetComponent<Image>();
    }

    public void ItemDelivered()
    {
        itemsCount--;
        if (itemsCount == 0)
        {
            StopCoroutine(progressRoutine);
            for (int i = 0; i < 6; i++)
            {
                transform.GetChild(1).GetChild(i).gameObject.SetActive(false); // disabling all the vegetables
            }


            int direction = 1;
            if (Random.value > 0.5f)
                direction *= -1;

            iTween.MoveTo(gameObject, iTween.Hash("position", transform.position + Vector3.right * 1000 * direction, "time", 4, "easetype", iTween.EaseType.linear, "oncomplete", "SitBack", "oncompletetarget", gameObject));
        }
    }

    private void StartWaitingForYourOrder()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        itemsCount = 0;
        bool atleastOneFlag = false;
        for (int i = 0; i < 6; i++)
        {
            if ((i == 5 && atleastOneFlag == false))
                goto customerNeed;
            else if (Random.value > 0.3)
                continue;
            customerNeed:
            atleastOneFlag = true;
            transform.GetChild(1).GetChild(i).gameObject.SetActive(true);
            itemsCount++;
        }

        progressRoutine = StartCoroutine(StartTimer(itemsCount));
        
    }

    private IEnumerator StartTimer(int count)
    {
        progressImage.fillAmount = 1;

        while (progressImage.fillAmount > 0)
        {
            yield return null;
            progressImage.fillAmount -= (Time.deltaTime / count * 0.01f);
        }

        transform.GetChild(0).gameObject.SetActive(false); // disabling progress bar

        for (int i = 0; i < 6; i++)
        {
            transform.GetChild(1).GetChild(i).gameObject.SetActive(false); // disabling all the vegetables
        }


        int direction = 1;
        if (Random.value > 0.5f)
            direction *= -1;

        iTween.MoveTo(gameObject, iTween.Hash("position", transform.position + Vector3.right * 1000 * direction, "time", 4, "easetype", iTween.EaseType.linear, "oncomplete", "SitBack", "oncompletetarget", gameObject));

    }

    private void SitBack()
    {
        transform.position = spawnPosition;

        gameObject.SetActive(false);
    }

}
