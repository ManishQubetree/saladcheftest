﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    [SerializeField]
    RectTransform leftLimit, rightLimit, upLimit, downLimit;

    public static GameManager instance = null;

    public Color choppedColor;

    [SerializeField]
    GameObject customerPrefab;

    [SerializeField]
    Transform customerArea;

    private void Awake()
    {
        instance = this;
    }

    private void Start()
    {
        Invoke("CustomerEnter", 1);
    }

    public Vector3 GetClampedPlayerPosition(Vector3 pos)
    {
        pos.x = Mathf.Clamp(pos.x, leftLimit.position.x, rightLimit.position.x);
        pos.y = Mathf.Clamp(pos.y, downLimit.position.y, upLimit.position.y);
        return pos;
    }

    private void CustomerEnter()
    {
        GameObject currentCustomer = null;

        string randomString = ScrambleWord("01234");
        for (int i = 0; i < randomString.Length; i++)
        {
            int index = int.Parse(randomString.Substring(i, 1));
            if (!customerArea.GetChild(index).gameObject.activeSelf)
            {
                currentCustomer = customerArea.GetChild(index).gameObject;
                break;
            }
        }

        if (currentCustomer != null)
        {
            currentCustomer.SetActive(true);
            Vector3 waitingPoint = currentCustomer.transform.position;
            if (Random.value > 0.5f)
                currentCustomer.transform.Translate(1000, 0, 0);
            else
                currentCustomer.transform.Translate(-1000, 0, 0);
            iTween.MoveTo(currentCustomer, iTween.Hash("position", waitingPoint, "time", 4, "easetype", iTween.EaseType.linear, "oncomplete", "StartWaitingForYourOrder", "oncompletetarget", currentCustomer));
        }

        Invoke("CustomerEnter", Random.Range(7, 15));


    }

    public string ScrambleWord(string word)
    {
        char[] chars = new char[word.Length];
        System.Random rand = new System.Random(10000);
        int index = 0;
        while (word.Length > 0)
        { // Get a random number between 0 and the length of the word. 
            int next = rand.Next(0, word.Length - 1); // Take the character from the random position 
                                                      //and add to our char array. 
            chars[index] = word[next];                // Remove the character from the word. 
            word = word.Substring(0, next) + word.Substring(next + 1);
            ++index;
        }
        return new System.String(chars);
    }
}
