﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cook : MonoBehaviour
{
    PlayerController pc;

    private void Start()
    {
        Invoke("CookingDone", Random.Range(2, 4));
    }

    public void SetPlayerController(PlayerController _pc)
    {
        pc = _pc;
    }

    private void CookingDone()
    {
        GetComponent<UnityEngine.UI.Image>().color = GameManager.instance.choppedColor;
        pc.SetChoppingDone();
        Destroy(this);
    }
}
